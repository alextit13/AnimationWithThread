package sample;

import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Controller {
    public Pane pane;
    public Button button;
    Rectangle rec1 = new Rectangle(50,50);


    public void buttonClick(MouseEvent mouseEvent) {
        pane.getChildren().clear();
        pane.getChildren().add(rec1);
        rec1.setX((pane.getWidth()/2)-rec1.getWidth()/2);
        rec1.setY((pane.getHeight()/2)-rec1.getHeight()/2);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("Поток проуснулся");
                try {
                    for (int i = 0; i < 10; i++) {
                        Thread.sleep(500);
                        Color color = new Color(ColorGeneration(),ColorGeneration(),ColorGeneration(),Opacity());
                        rec1.setFill(color);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Поток заснул");
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public double ColorGeneration(){
        double color = (Math.random()*255);
        System.out.println("color " + color/1000);
        return color/1000;
    }
    public double Opacity(){
        double opacity = (Math.random());
        System.out.println("opacity " + opacity);
        return opacity;
    }

    public void Navedena(MouseEvent mouseEvent) {
        button.setTextFill(Color.GREEN);
        button.setScaleX(1.3);
        button.setScaleY(1.3);
    }

    public void Ubrana(MouseEvent mouseEvent) {
        button.setTextFill(Color.BLACK);
        button.setScaleX(1);
        button.setScaleY(1);
    }
}

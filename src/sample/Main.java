package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Animation");
        primaryStage.setScene(new Scene(root, 250, 250));
        primaryStage.setMaxWidth(250);
        primaryStage.setMaxHeight(250);
        primaryStage.setMinWidth(250);
        primaryStage.setMinHeight(250);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
